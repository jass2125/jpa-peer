package io.github.jass2125.jpa.peer1;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anderson Souza
 * @email jair_anderson_bs@hotmail.com
 * @since 2015, Feb 1, 2016
 */
public class Loader {
    
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("exemplo");
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        em.persist(new Pessoa("Anderson", 19));
        em.getTransaction().commit();
        em.close();
        
    }

}
